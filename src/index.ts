import twColors from "tailwindcss/colors";
import Color from "colorjs.io";

import brands from "./data.json";

// https://colorjs.io/docs/color-difference#delta-e-e
const JND = 2.3;

function main() {
  // console.log(brandColors);
  // console.log(twColors);
  // console.log(Color.deltaE("red", "blue", "2000"));

  for (const twColorName in twColors.purple) {
    const twColor = twColors.red[twColorName];

    for (const brand of brands) {
      const brandColor = brand.color;
      const colorDiff = Color.deltaE(twColor, brandColor, "2000");

      if (colorDiff < JND) {
        console.log(colorDiff);
      }
    }
  }
}

main();
