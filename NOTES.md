# Notes

- https://stackoverflow.com/a/59098402
- https://tailwindcss.com/docs/customizing-colors#using-the-default-colors
- https://tailwindcss.com/docs/customizing-colors
- https://www.npmjs.com/package/colorjs.io
- https://techkonusa.com/cie-%CE%B4e-color-difference-equations/:
  - "(...) (JND) of deltaE is usually 1. In other words, if two colors have a deltaE less than 1 it is imperceptible and larger than 1 is perceivable."
- https://en.wikipedia.org/wiki/Color_difference#CIELAB_%CE%94E*

## Commands

```bash
npm install tailwindcss colorjs.io && npm install -D prettier jiti
```

```bash
npm install -D "@types/node@$(cat .nvmrc | cut -d . -f 1-2)"
```
