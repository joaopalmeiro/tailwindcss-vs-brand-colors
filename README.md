# tailwindcss-vs-brand-colors

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version
```

```bash
npm install
```

```bash
curl -o src/data.json https://codeberg.org/joaopalmeiro/brand-colors/raw/branch/main/data.json
```

```bash
npm run dev
```

```bash
npm run format
```
